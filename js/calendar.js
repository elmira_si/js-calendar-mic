
var prefix = "calStorageData-";  // Local storage Prefix

var day_of_week = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var month_of_year = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

//  DECLARE AND INITIALIZE VARIABLES
var Calendar = new Date();

var year = Calendar.getFullYear();  // .getYear() Returns year
var month = Calendar.getMonth();    // Returns month (0-11)
var today = Calendar.getDate();     // Returns day (1-31)
var weekday = Calendar.getDay();    // Returns day (1-31)

var DAYS_OF_WEEK = 7;       // "constant" for number of days in a week
var DAYS_OF_MONTH = 31;    // "constant" for number of days in a month

var cal;    // Used for print calendar

Calendar.setDate(1);    // Start the calendar day at '1'
Calendar.setMonth(month);    // Start the calendar month at now

function singleCal() {

    // MONTH AND YEAR
    cal = "<div class='calHeader'>" + month_of_year[month] + "&nbsp;" + year + "</div>";

    // LOOPS FOR WEEK NAMES CALENDAR  
    cal += "<div class='calBody'><div class='weekRow'>";
    for (var i = 0; i < DAYS_OF_WEEK; i++) {
        if (weekday == i) {
            cal += "<span class='calWeekDays calToday'>" + day_of_week[i] + "</span>";
        } else {
            cal += "<span class='calWeekDays'>" + day_of_week[i] + "</span>";
        }
    }
    cal += "</div>";

    // LOOPS FOR EACH DAY IN CALENDAR 
    cal += "<div class='calMouth'>";

    for (i = 0; i < Calendar.getDay(); i++) {
        cal += "<span class='emptyDays'>&nbsp;</span>";
    }



    for (i = 0; i < DAYS_OF_MONTH; i++) {


        if (Calendar.getDate() > i) {
            week_day = Calendar.getDay();
            // START NEW ROW FOR FIRST DAY OF WEEK
            if (week_day == 0) {
                cal += "<div></div>";
            }
            if (week_day != DAYS_OF_WEEK) {
                var day = Calendar.getDate();

                var id_days = day + "_" + month + "_" + year;

                var idDay = prefix + day + "/" + month + "/" + year;


                if (today == day) {
                    cal += "<span ondrop='drop(event)' ondragover='allowDrop(event)' class='calDays calToday' id= '" + id_days + "' onclick='addEventBox(this.id);'>" + day;

                    for (var i = 0; i < localStorage.length; i++) {
                        var key = localStorage.key(i);
                        var getValue = localStorage.getItem(key);
                        if (key == idDay) {
                            cal += "<p id='dragId_" + id_days + "' draggable='true' ondragstart='drag(event)'>" + getValue + "</p>";
                        }
                    }
                    cal += "</span>";
                } else {
                    cal += "<span ondrop='drop(event)' ondragover='allowDrop(event)' class='calDays' id= '" + id_days + "' onclick='addEventBox(this.id);'>" + day;
                    for (var j = 0; j < localStorage.length; j++) {
                        var key = localStorage.key(j);
                        var getValue1 = localStorage.getItem(key);
                        if (key == idDay) {
                            cal += "<p id='dragId_" + id_days + "' draggable='true' ondragstart='drag(event)'>" + getValue1 + "</p>";
                        }
                    }
                    cal += "</span>";
                }

            }
        }
        // INCREMENTS UNTIL END OF THE MONTH
        Calendar.setDate(Calendar.getDate() + 1);

    }
    cal += "</div></div>";

    return cal;
}


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("Text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("Text");
    ev.target.appendChild(document.getElementById(data));
}

///////////////////////////////
function getDayEvent(idDay) {

    for (var i = 0; i < localStorage.length; i++) {

        var key = localStorage.key(i);
        var getValue = localStorage.getItem(key);

        var getDayEvent = '';
        if (key == idDay) {
            //console.log(key);
            return  getValue;

        } else {
            return '';
        }

    }

}/////////////////////////

function setValue(id, val) {
    return document.getElementById(id).innerHTML = val;
}

function closeWin() {
    setValue('addEvWin', '');
}

function addEventBox(dayId) {

    setValue('addEvent', '');

    for (var i = 0; i <= dayId.length; i++) {
        dayId = dayId.replace('_', '/');
    }

    var newEv = '<div id="addEvWin">\n\
                <div class="addEventInput1"> <p class="closeAddWin" onclick="closeWin();">X</p>\n\
                <p class="title">Add title</p>\n\
                <p><input type="text" class="addDate" id="addDate" placeholder="add date" value="' + dayId + '"/></p>\n\
                <p><input type="text" class="addEventTitle" id="addEventTitle" placeholder="add title"/></p>\n\
                <p class="saveEvent" id="saveData" onclick="saveData();">Save Event</p></div>';

    setValue('addEvent', newEv);

}

function saveData() {

    if (localStorage) { // LocalStorage is supported!

        var key = document.getElementById('addDate').value;
        var value = document.getElementById('addEventTitle').value;
        localStorage.setItem(prefix + key, value);

        var dayId = key;
        for (var i = 0; i <= dayId.length; i++) {
            dayId = dayId.replace('/', '_');
        }

        document.getElementById('data').innerHTML = getFromStorage();
        document.getElementById(dayId).innerHTML += "<p id='dragId_" + dayId + "' draggable='true' ondragstart='drag(event)'>" + value + "</p>";
    } else {
        alert('No support for Local Storage');
    }

}
function getFromStorage() {

    var inHtm = '';

    for (var i = 0; i < localStorage.length; i++) {    //******* length

        var key = localStorage.key(i);              //******* key()			
        getValue = localStorage.getItem(key);		//****** get value by key

        if (key.indexOf(prefix) == 0) {

            var shortkey = key.replace(prefix, "");
            inHtm += "<li><span class='itemDate'>" + shortkey + "</span> " + getValue + "<span class='itemDel' id=" + key + " onclick='deleteItem(this.id);'>X</span></li>";
        }
    }

    return inHtm;
}

function deleteItem(id) {

    var dayId = id;
    dayId = dayId.replace("calStorageData-", "");

    for (var i = 0; i <= dayId.length; i++) {
        dayId = dayId.replace('/', '_');
    }


    localStorage.removeItem(id);

    var el = document.getElementById('dragId_' + dayId);
    el.parentNode.removeChild(el);

    document.getElementById('data').innerHTML = getFromStorage();
    //document.getElementById('dragId_' + dayId);
}
function removeAll() {
    //alert("remove");

    for (var i = 0; i < localStorage.length; i++) {    //******* length

        var dayId = localStorage.key(i);              //******* key()		

        dayId = dayId.replace('calStorageData-', '');
        for (var i = 0; i <= dayId.length; i++) {
            dayId = dayId.replace('/', '_');
        }

        //document.getElementById('dragId_' + dayId).innerHTML = '';
        /*if(key.indexOf(prefix) == 0) {
         localStorage.removeItem(key);
         alert(key);
         }*/
        //document.getElementById(key).innerHTML = '';
    }
    localStorage.clear();
    document.getElementById('data').innerHTML = '';


}

function saveChanges() {
    alert("save");

}
function showTitle() {

    var title = "<h4> All Events </h4><div id='data'><span class='noTitle'>There is no events. <br />Click to Days for add new event. <span></div>";

    return title;
}

function showEvents(id) {

    var inHtm = '';
    var key = localStorage.key(prefix + id);      //******* key()			
    getValue = localStorage.getItem(key);		//****** get value by key

    return inHtm;
}

document.getElementById('calendar').innerHTML = singleCal();
document.getElementById('events').innerHTML = showTitle();
document.getElementById('data').innerHTML = getFromStorage();